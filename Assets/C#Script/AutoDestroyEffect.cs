﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroyEffect : MonoBehaviour {
    ParticleSystem particle;
    void Start () {
        //省略されているが正式に書くとthis.GetComponent<ParticleSystem>()
        //自分自身が持っているParticleSystemを取得
        particle = GetComponent<ParticleSystem>();
    }
    void Update () {
        //パーティクルの再生が終了したらGameObjectを削除
        //isPlayingはプレイされていたらtrueという意味
        if (particle.isPlaying == false) Destroy(gameObject);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pusher : MonoBehaviour {
    Vector3 startPosition;
    //移動量パラメータ
    public float amplitude;
    //移動速度パラメータ
    public float speed;
    void Start () {
        //初期値の保存
        startPosition = transform.localPosition;
    }
    void Update () {
        //変位を計算(移動量の計算)
        float z = amplitude * Mathf.Sin(Time.time * speed);
        //zを変位させたポジションに再設定 ポジションの反映
        transform.localPosition = startPosition + new Vector3(0, 0, z);
    }
}

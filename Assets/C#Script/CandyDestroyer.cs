﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CandyDestroyer : MonoBehaviour {
    public CandyHolder candyHolder;
    public int reward;
    //エフェクトプレファブパラメータ
    public GameObject effectPrefab;
    //エフェクトローテーションパラメータ Vector3は引数３つ
    public Vector3 effectRotation;
    private void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Candy")
        {
            //指定数だけCandyのストックを増やす
            candyHolder.AddCandy(reward);
            //オブジェクトの削除
            Destroy(other.gameObject);
            //エフェクトプレファブ(☆が舞う)の設定チェック
        //CandyDestroyerReward0にはエフェクトプレファブは設定されていないのでnull
        //CandyDestroyerReward3にはエフェクトプレファブが設定されているためnullではなくなる
            if (effectPrefab != null)
            {
                //Candyのポジションにエフェクトを生成
                //Instantiate()はプレファブを生成する
                Instantiate(
                //生成するプレファブ
                effectPrefab,
                //キャンディが落ちた位置
                other.transform.position,
                //Quaternion.Euler(クォータニオン.オイラー)は向きを決める関数
                //Vector3で設定した向きを設定
                Quaternion.Euler(effectRotation));
            }
        }
    }

}

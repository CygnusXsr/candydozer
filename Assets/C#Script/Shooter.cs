﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour {
    const int SphereCandyFrequency = 3;
    const int MaxShotPower = 5;
    //ショットパワーの回復時間定数
    const int RecoverySeconds = 3;
    int sampleCandyCount;
    int shotPower = MaxShotPower;
    AudioSource shotSound;
    //ゲームオブジェクトを入れる配列
    public GameObject[] candyPrefabs;
    public GameObject[] candySquarePrefabs;
    public CandyHolder candyHolder;
    //Candyプレファブパラメータ
    public float shotSpeed;
    public float shotTorque;
    public float baseWidth;
    private void Start()
    {
        shotSound = GetComponent<AudioSource>();
    }
    void Update () {
        //入力の検知
        if (Input.GetButtonDown("Fire1")) Shot();
    }
    //キャンディのプレファブからランダムに１つ選ぶ
    GameObject SampleCandy()
    {
        GameObject prefab = null;
        //３回に一回丸いキャンディを選択する
        if(sampleCandyCount % SphereCandyFrequency == 0)
        {
            //Random.Range(最小値,最大値)で最小値から最大値までの数字がランダムで生成
            int index = Random.Range(0, candyPrefabs.Length);
            prefab = candyPrefabs[index];
        }
        else
        {
            int index = Random.Range(0, candySquarePrefabs.Length);
            prefab = candySquarePrefabs[index];
        }
        sampleCandyCount++;
        return prefab;
    }
    Vector3 GetInstantiatePosition()
    {
        //画面のサイズとInputの割合からキャンディ生成のポジションを計算 発射位置の計算
        float x = baseWidth * (Input.mousePosition.x / Screen.width) - (baseWidth / 2);
        return transform.position + new Vector3(x, 0, 0);
    }
    public void Shot()
    {
        //キャンディを生成できる条件外ならばShotしない
        if (candyHolder.GetCandyAmount() <= 0) { return; }
        //ショットパワーのチェック
        if(shotPower <= 0) { return; }
        //プレファブからCandyオブジェクトを生成
        //生成プレファブと生成位置の修正
        GameObject candy = (GameObject)Instantiate(
                                    SampleCandy(),
                                    GetInstantiatePosition(),
                                    Quaternion.identity
                                    );
        //生成したCandyオブジェクトの親をCandyHolderに設定する
        candy.transform.parent = candyHolder.transform;
        //CandyオブジェクトのRigidbodyを取得し力回転を加える
        Rigidbody candyRigidBody = candy.GetComponent<Rigidbody>();
        //力とベクトルの加算
        candyRigidBody.AddForce(transform.forward * shotSpeed);
        candyRigidBody.AddTorque(new Vector3(0, shotTorque, 0));
        //Candyのストックを消費
        candyHolder.ConsumeCandy();
        //ShotPowerを消費
        ConsumePower();
        //サウンドを再生
        shotSound.Play();
    }
    private void OnGUI()
    {
        GUI.color = Color.black;
        //ShotPowerの残数を+の数で表示
        string label = "";
        for(int i = 0; i < shotPower; i++) { label = label + "+"; }
        GUI.Label(new Rect(0, 15, 100, 30), label);
    }
    void ConsumePower()
    {
        //ShotPowerを消費すると同時に回復のカウントをスタート
        shotPower--;
        StartCoroutine(RecoverPower());
    }
    IEnumerator RecoverPower()
    {
        //一定秒数待った後にshotPowerを回復
        yield return new WaitForSeconds(RecoverySeconds);
        shotPower++;
    }
}
